let gulp 	 	 = require('gulp'),
	sass 	 	 = require('gulp-sass'),
	uglify 	 	 = require('gulp-uglify'),
	cleancss 	 = require('gulp-clean-css'),
	autoprefixer = require('gulp-autoprefixer'),
	babel 		 = require('gulp-babel'),
	browserify 	 = require('browserify'),
	source 		 = require('vinyl-source-stream'),
	buffer		 = require('vinyl-buffer'),
	browserSync  = require('browser-sync')

gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'public'
		},
		notify: false,
	})
});

gulp.task('styles', function () {
	return gulp.src('frontend_src/sass/**/*.sass')
	.pipe(sass())
	.pipe(autoprefixer({
		level: {
			1: {
				specialComments: 0
			}
		}
	}))
	// .pipe(cleancss())
	.pipe(gulp.dest('public/css'))
	.pipe(browserSync.stream())
})

gulp.task('scripts', function () {
	return browserify({
		entries: 'frontend_src/js/main.js',
    	debug: true
	})
	.bundle()
	.pipe(source('main.js'))
	.pipe(buffer())
	.pipe(babel({
		presets: ['@babel/env']
	}))
	.pipe(uglify())
	.pipe(gulp.dest('public/js'))
	.pipe(browserSync.reload({ stream: true }))
})

gulp.task('watch', function () {
	gulp.watch('frontend_src/sass/**/*.sass', gulp.parallel('styles'))
	gulp.watch('frontend_src/js/**/*.js', gulp.parallel('scripts'))
})

gulp.task('default', gulp.parallel('styles', 'scripts', 'browser-sync', 'watch'))