let logEl = document.querySelector('input[name="log"]')
let passEl = document.querySelector('input[name="pass"]')

let btnEl = document.querySelector('.js-send-btn')

logEl.addEventListener('input', function() {
    if (logEl.value && passEl.value) {
        btnEl.disabled = false
    } else {
        btnEl.disabled = true
    }
})

passEl.addEventListener('input', function() {
    if (logEl.value && passEl.value) {
        btnEl.disabled = false
    } else {
        btnEl.disabled = true
    }
})